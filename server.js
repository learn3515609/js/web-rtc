var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');
var peers = [];
var peerSPDs = [];
app.listen(8111);
function handler(req, res) {
    fs.readFile("".concat(__dirname, "/index.html"), function (error, data) {
        if (error) {
            res.writeHead(500);
            return res.end('index file not found');
        }
        res.writeHead(200, { 'Content-type': 'text/html' });
        res.end(data);
    });
}
io.sockets.on('connection', function (socket) {
    console.log('peer connected');
    if (peers.length === 0) {
        socket.emit('client_socket', '0');
        peers.push(socket);
    }
    else if (peers.length === 1) {
        socket.emit('client_socket', peerSPDs[0]);
        peers.push(socket);
    }
    socket.on('server_socket', function (data) {
        if (peers.length < 3) {
            peerSPDs.push(data);
            socket.broadcast.emit('client_socket', data);
        }
    });
    socket.on('disconnect', function () {
        peers = [];
        peerSPDs = [];
        var sockets = io.sockets.clients();
        for (var i = 0; i < sockets.length; i++) {
            sockets[i].emit('client_socket', 'reset');
        }
    });
});
