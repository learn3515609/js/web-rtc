const app = require('http').createServer(handler);
const io = require('socket.io')(app);
const fs = require('fs');

let peers: any[] = [];
let peerSPDs: any[] = [];

app.listen(8111);

function handler(req, res) {
  fs.readFile(`${__dirname}/index.html`, (error, data) => {
    if(error) {
      res.writeHead(500);
      return res.end('index file not found');
    }
    res.writeHead(200, {'Content-type': 'text/html'});
    res.end(data);
  })
}

io.sockets.on('connection', (socket) => {
  console.log('peer connected');

  if(peers.length === 0) {
    socket.emit('client_socket', '0');
    peers.push(socket);
  } else if (peers.length === 1) {
    socket.emit('client_socket', peerSPDs[0]);
    peers.push(socket);
  }

  socket.on('server_socket', (data) => {
    if(peers.length < 3) {
      peerSPDs.push(data);
      socket.broadcast.emit('client_socket', data);
    }
  })

  socket.on('disconnect', () => {
    peers = [];
    peerSPDs = [];
    let sockets = io.sockets.clients();
    for(let i = 0; i < sockets.length; i++) {
      sockets[i].emit('client_socket', 'reset');
    }
  })
})